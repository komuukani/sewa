/*
 [Custom theme javascript]
 
 Project:  Elevate Modern Business vCard
 URL: https://nishantthummar.tech/
 Author: Nishant Thummar <nishantthummar005@gmail.com>
 Version: 1.0 
 Last update:  01/09/22   
 
 [Table of contents]
 
 1. WOW ANIMATION
 2. THEME SWITCHER (DARK/LIGHT)
 3. CURSOR EFFECT
 4. GALLERY FILTER
 5. SHARE ON WHATSAPP BUTTON
 6. SHARE BUTTON
 7. DIRECT WHATSAPP SHARE BUTTON
 8. ACTIVE MENU
 9. BOOTSTRAP MENU (TOGGLE ON CLICK)
 */

// Use Strict Mode
$(document).ready(function () {
    $(document).on("click", ".commonClickEvent", function (){
     $(".commonSelectionBlck").hide();
     var attrName = $(this).data("showname");
     console.log(attrName);
        $("."+attrName).show();
        console.log($("."+attrName));
    });
});
